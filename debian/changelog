emacs-anzu (0.67-1) unstable; urgency=medium

  * New upstream version 0.67
  * Add patch to fix version number
  * Add upstream metadata
  * Refresh patch to clean README.md
  * d/control: Declare Standards-Version 4.7.0 (no changes needed)
  * d/copyright: Update copyright information

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 10 Nov 2024 12:59:00 +0500

emacs-anzu (0.64-1) unstable; urgency=medium

  * New upstream version 0.64
  * Drop patch to fix version number
  * Refresh patch to clean documentation
  * d/control: Declare Standards-Version 4.5.1 (no changes needed)
  * d/rules: Install upstream changelog

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 06 Dec 2020 20:30:38 +0500

emacs-anzu (0.63-1) unstable; urgency=medium

  [ David Krauser ]
  * d/control: Update maintainer email address

  [ Lev Lamberov ]
  * New upstream version 0.63
  * Add patch to fix version number
  * Refresh patch to clean documentation
  * Migrate to debhelper-compat 13 (without d/compat)
  * d/control: Declare Standards-Version 4.5.0 (no changes needed)
  * d/control: Add Rules-Requires-Root: no
  * d/control: Clean dependencies (drop emacs24, emacs25)
  * d/control: Drop Built-Using
  * d/copyright: Update copyright information
  * d/copyright: Fix Upstream-Name and add Upstream-Contact
  * d/rules: Do not pass --parallel to dh

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 14 May 2020 11:56:01 +0500

emacs-anzu (0.62-4) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 14:24:59 -0300

emacs-anzu (0.62-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 08:45:58 -0300

emacs-anzu (0.62-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 20:13:50 -0300

emacs-anzu (0.62-1) unstable; urgency=low

  * Initial release (Closes: #847707)

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 10 Dec 2016 23:47:28 +0500
